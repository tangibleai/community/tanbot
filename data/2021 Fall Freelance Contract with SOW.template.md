# {company_name} {project_name}

Hi {name},

{prologue}We are pleased to offer you a freelance contract at {company_name}!
Your project starts *{start_date}* and ends *{end_date}*.

Read the contract below and fill out your name and address at the end.
Check the checkboxes at the end of this agreement so that we know you understand the key points of this agreement.
To mark a checkbox, put an "X" or "x" between the square brackets ("[ ]").

To electronically sign this {project_name} Agreement, reply to this email and edit the included agreement. Overwrite the three bolded placeholders in the checklist at the bottom of the agreement with your information: __FULL_NAME__, __EMAIL_ADDRESS__, and __DATE_SIGNED__ .

-------------------------------------------------------------------------------

# {company_name} {cohort_name} {project_name} Agreement

This {project_name} Agreement is between {company_name} LLC ("{company_name}") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {full_name} ("{name}") at {email}.

The purpose of this {project_name} is to allow {name} to gain professional experience working on a real-world project while contributing to making the world a better place (positive social impact) according to the following terms and conditions:

### 1. Term:

The {project_name} begins *{start_date}* and concludes *{end_date}*, unless the {project_name} ends earlier
according to other provisions in this agreement.

### 2. Purpose:

During the Term, {company_name} will provide {name} with mentorship and guidance on completing their project tasks. {company_name} will continuously monitor {name}'s progress and assist with implementation where required.

### 3. Responsibilities:

During the Term, {name}'s duties and responsibilities include:

    (a) Maintaining good academic standing with your educational institution
    (b) Demonstrating honesty, courtesy, kindness, and a super-cooperative attitude
    (c) Provide regular (at least weekly) updates on progress in writing (slack or e-mail)
    (d) Invoice {company_name} for hours worked on accomplishing the tasks below ("Section 4. Tasks")

### 4. Tasks:

{name} and {company_name} will jointly establish a schedule to accomplish the tasks indicated in the [SOW (Statement of Work)]({sow_url}).

### 5. Intern position:

{name} acknowledges and agrees that he or she is not engaging in the {project_name} as an employee and is not entitled to receive any wages for work performed or services supplied to {company_name}. 
Further {name} is responsible for all costs associated with traveling to and from {company_name} or as otherwise required in the performance of the {project_name}.
Nonetheless a stipend of $500 per month will be paid to {name} in support of {name}'s scholarship. 

### 6. Acknowledgments and Representations:

    (a) {name} is not entering into this {project_name} for the purpose of pursuing a livelihood.
    (b) There is no expectation that the {project_name} will result in {company_name} employing {name}.
    (c) {company_name} agrees that {name} will not replace or displace any employee of {company_name}.
    (d) {company_name} will expend resources educating {name}. Those resources will exceed any benefit to {company_name}'s business activities.

### 7. Termination:

{company_name} or {name} may terminate the {project_name} without notice.

### 8. Default to Open:

{company_name} defaults to open. Unless restricted by customer contracts or fiducial responsibility to protect user data, all {company_name} software, concepts, data and designs are licensed under the free and open source [Hippocratic (MIT + First Do No Harm)](https://firstdonoharm.dev/) License. Interns, alumni, employees, freelance contractors, and public contributors may freely share and reuse these designs and software packages in future projects. Listed here are examples of free open source software packages that {name} may contribute to without reservation, knowing they may share their work freely with others after conclusion of this {project_name}:

a) [`qary`](http://gitlab.com/tangibleai/qary)
b) [`tanbot`](http://gitlab.com/tangibleai/team)
c) [team resources](http://gitlab.com/tangibleai/team)
d) [`nlpia`](http://gitlab.com/tangibleai/nlpia)
e) [`nessvec`](http://gitlab.com/tangibleai/nessvec)

### 9. User Data:

In the course of performing their duties under the agreement {name} might have access to data from the users interacting with {company_name}'s Products ("User Data").
All right, title, and interest in User Data will remain the property of {company_name}, unless otherwise contracted. {name} has no intellectual property rights or other claim to User Data that is hosted, stored, or transferred to and from the Products operated or maintained on behalf of {company_name}. {name} will cooperate with {company_name} to protect the User Data and the users’ right to privacy. {name} will promptly notify {company_name} if {pronoun_is__lower} aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

{name} acknowledges and agrees that, in the course of performing their duties under the agreement, {name} may receive or have access to Personal Information. {name} shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control
In recognition of the foregoing, {name} agrees to:

    (a) keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
    (b) use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for {name}'s own purposes without Customer's prior written consent; and
    (c) not, directly or indirectly, disclose Personal Information to any person other than {company_name}'s authorized persons, including any subcontractors, agents, outsourcers or auditors (an "Unauthorized Third Party"), without express written consent from {company_name}, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

{name} represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives.
{name} shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, {name}'s safeguards for the protection of Personal Information shall include:

    (a) securing personal computer limiting access of Personal Information to Authorized Persons securing information transmission, storage and disposal; and
    (b) implementing authentication and access controls within applications that are used to store and access the Personal Information

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

----

- [x] Agreed to by {manager_name} ({manager_email}) on {manager_date_signed}

----

- [ ] Agreed to by __FULL_NAME__ (__EMAIL_ADDRESS__) on __DATE_SIGNED__
- [ ] I want to learn about the activities and technology of {company_name} and help others with what I learn.
- [ ] I have a password-protected PC I will use to store {company_name} data. Automatic screen lock is enabled.
- [ ] I will use a browser, such as Firefox, with at least **_Standard_ level security** protections enabled.
- [ ] I agree to make my best effort to attend at least one meeting with a {company_name} representative weekly.

