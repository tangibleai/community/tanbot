Hi {{name}}!

{{preamble}}  

We are pleased to offer you a paid internship at Tangible AI! We will pay you **${{dollars}}/hr** through UpWork for up to **{{hours}} hr/wk** for the next **{{weeks}} weeks**. I will update the UpWork project description. Thank you for being generous with your time and contributing your code to these open source projects as part of your learning. I will try to continue to help you expand your skills in software development and NLP.  

Whether or not you accept the internship, you are welcome to join us in the public communities listed in our ["Team" GitLab repository](https://gitlab.com/tangibleai/team/-/tree/main/exercises/1-social)

If you would like to join us for this internship read the contract below and fill out your name and address at the end. Mark the checkboxes at the end of this agreement with an "X" between the square brackets ("\[ \]"). This helps us know that you understand the key points of this agreement.

To electronically sign this agreement, reply to this email and edit the included agreement. Overwrite the 3 placeholders in the checklist at the bottom of the agreement with your information: `__FULL_NAME__`, `__EMAIL_ADDRESS__`, and `__DATE_SIGNED__`.

___

# Tangible AI Internship Agreement

This Internship Agreement ("Agreement") is between Tangible AI, LLC LLC ("Tangible AI") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {{full_name}} ("{{name}}") at [{{email}}](mailto:{{email}}).

The purpose of this Internship is to allow {{name}} to gain professional software development experience working on real-world social impact projects, according to the following terms and conditions:

### 1. Term:

{{name}}'s Internship ("Internship") begins _Dec 9, 2022_ and concludes Feb 3, _2023_, unless the Internship is terminated by either party.

### 2. Purpose:

During the Term, Tangible AI will provide {{name}} with mentorship and learning opportunities. Tangible AI will continuously adjust the activities and assignments of {{name}} according to mutually agreed upon goals.

### 3. Responsibilities:

During the Term, {{name}}'s duties and responsibilities include:

1.  Demonstrating honesty, punctuality, courtesy, curiosity, and a prosocial attitude; and

2.  Engaging in the learning opportunities that Tangible AI provides, to the best of {{name}}'s abilities;

3.  Provide Tangible AI weekly verbal or text message reports on their progress.

### 4. Tasks:

{{name}} and Tangible AI will jointly establish a schedule of meetings and tasks during the term of the Internship.

### 5. Intern position:

{{name}} acknowledges and agrees that they are not a permanent employee of Tangible AI but are participating in a temporary Internship for education.

### 6. Acknowledgements and Representations:

1.  There is no expectation that the Internship will result in Tangible AI employing {{name}} permanently.

2.  Tangible AI agrees that {{name}} will not replace or displace any employee of Tangible AI.

3.  Tangible AI will expend resources educating {{name}}. Those resources may exceed any benefit to Tangible AI's business activities.

### 7. Termination:

Tangible AI and {{name}} both retain the right to terminate the Internship without advance notice.

### 8. Default to Open:

Tangible AI **"defaults to open"**. Unless restricted by customer contracts or fiducial responsibility to protect user data, all Tangible AI software, concepts, data and designs are licensed under the free and open source licenses (in order of preference):

1.  HL3: [Hippocratic License 3.0](https://firstdonoharm.dev/)
2.  AGPL3: [GNU Affero General Public License v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
3.  MIT: [The MIT License](https://opensource.org/licenses/MIT)
4.  GPL3: [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)

All our software is hosted on **GitLab**: [gitlab.com/tangibleai/](https://gitlab.com/tangibleai/)

### 9. User Data:

In the course of performing their duties under the Agreement, {{name}} might have access to data from the users interacting with Tangible AI's products or services ("User Data"). All right, title, and interest in User Data will remain the property of Tangible AI, unless otherwise contracted. {{name}} has no intellectual property rights or other claim to User Data except where anonymized and released under an open source license by Tangible AI. {{name}} will cooperate with Tangible AI to protect the User Data and the users' right to privacy. {{name}} will promptly notify Tangible AI if {{name}} becomes aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

{{name}} acknowledges and agrees that, in the course of performing their duties under the Agreement, {{name}} may receive or have access to Personal Information. {{name}} shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control In recognition of the foregoing, {{name}} agrees to:

1.  keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
2.  use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for {{name}}'s own purposes without Customer's prior written consent; and
3.  not, directly or indirectly, disclose Personal Information to any person other than Tangible AI's authorized persons, including any subcontractors, agents, outsourcers or auditors (an "Unauthorized Third Party"), without express written consent from Tangible AI, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

{{name}} represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives. {{name}} shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, {{name}}'s safeguards for the protection of Personal Information shall include:

1.  securing a personal computer to limit access of Personal Information to Authorized Persons; and
2.  securing information transmission, storage and disposal of Personal Information; and
3.  implementing authentication and access controls within applications that are used to store and access the Personal Information.

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

___

-   [x]  Agreed to by Hobson Lane ([hobson@tangibleai.com](mailto:hobson@tangibleai.com)) on {{mentor_signed_date}}

___

-  [ ] Agreed to by `__FULL_NAME__` (`__EMAIL_ADDRESS__`) on `__DATE_SIGNED__`
-  [ ] I want to learn how to use technology for a positive impact on society.
-  [ ] I will use a password protected PC with automatic screen lock enabled to store Tangible AI data.
-  [ ] The web browser I use, such as Firefox or LibreWolf, will have at least **_Standard_ level security** protections enabled.
-  [ ] I agree to make my best effort to attend at least one meeting with a Tangible AI representative weekly.
-  [ ] Throughout the Internship I will be kind and helpful to others at Tangible AI and within communities Tangible AI supports.