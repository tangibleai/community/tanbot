# Welcome to the {cohort_name} Internship {name}

Hi {name},

Welcome aboard!

Your first assignment is to create a 30 minute one-on-one weekly meeting with me (Hobson) on [my calendar](https://calendly.com/hobs).

Next you'll want to add at least one of our weekly mob programming sessions to your calendar:

* Monday's at 11 AM PT (California) or 9 PM EEST (Kyiv) on [Hobson's Zoom](https://ucsd.zoom.us/my/hobsonlane) and [Manning's Twitch](https://twitch.tv/manningpublications)
* Wednesday's at 5:30 PM Pacific 3:30 AM in EEST (Kyiv) on [Hobson's Zoom](https://ucsd.zoom.us/my/hobsonlane) and [Manning's Twitch](https://twitch.tv/manningpublications)

You can collaborate with Tangible AI staff and other interns on the [San Diego Machine Learning slack workspace](https://proai.org/sdml-slack) in the `#qary` channel.

## Optional Workspaces

Here are some Slack channels to join if you want to collaborate with others working on Python or social-impact projects.

- [SD Python Slack](https://proai.org/sdpug-slack)
- [NextSD Slack](https://proai.org/nextsd-slack)

## Advanced Social Media

For a more prosocial social media experience, you may want to join the ecosystem of open source nonprofit social media platforms.

* Microblogging with [Mastodon](https://mastodon.social/)
* Helpful [Mastodon tutorial](https://chrisbeckstrom.com/tutorials/get_started_with_mastodon/)
* Hobson's [Mastodon profile](https://mastodon.social/@hobson) 
* Private Group Messaging with [Telegram](https://telegram.org/)
* Private Group Messaging with [Matrix](https://matrix.org/)
* Other [ActivityPub Clients](https://en.wikipedia.org/wiki/ActivityPub#Notable_implementations)


## Gitlab

You'll need a GitLab account to be able to share your code with the world.
GitLab is 100% open source and socially responsible, unlike GitHub (Microsoft).

1. Sign up with a free account on gitlab.com (no need to chose any of the fancy trial subscriptions)
2. Star & fork the `team` repo: [gitlab.com/tangibleai/team](https://gitlab.com/tangibleai/team)
3. Check out previous intern [project reports]({intern_reports_url})
4. Star & fork the `qary` repo (https://gitlab.com/tangibleai/qary)
5. Check out the [`qary` docs](https://docs.qary.ai)

### More GitLab Repos

Star & fork any of [our repositories](https://gitlab.com/tangibleai/) that interest you:

- [`nessvec`](https://gitlab.com/tangibleai/nessvec): an NLP project
- [`nudger`](https://gitlab.com/tangibleai/nudger): an SMS chatbot for community college students
- [`tanbot`](http://gitlab.com/tangibleai/tanbot): a Telegram and e-mail bot for social impact (and sending interns e-mails ;)
- [`nlpia2`](https://gitlab.com/tangibleai/nlpia2) (ask us about the private `nlpia-manuscript` repo)

## Zoom Meetings

Here are some Google Calendar invitations for meetings you can attend to see how an Agile organization works:




