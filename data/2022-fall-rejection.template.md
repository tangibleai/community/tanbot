# {company_name} Jr Developer Application

Hi {name},

Thank you taking the time to apply for the {company_name} Tan out of your stressful life to interview with me last week. Unfortunately, the interview I had with another candidate went really well and we are offering him the one remaining opening we had available.

For your information, we used gitlab.com/tangibleai/maiteam (machine learning) to evaluate applications for 4 qualities:
    1. English proficiency
    2. technical skill
    3. problem solving
    4. discipline

I reviewed the top 50% of applications myself and your application ranked #2 out of approximately 150 applications after human evaluation of those applications.

We interviewed 6 candidates and you ranked number 5 after these interviews.
We hired the top 4 of those 6 candidates for test projects, so you are next on our list.
You will be the first person we think of should we need additional developers in the future.
And I'm available for mentorship and assistance on any projects related to our mission - social impact chatbots (including mental health coaches and virtual assistance bots in Ukraine).

For future openings at Tangible AI we will prioritize applicants that have contributed Merge Requests or feature suggestions and bugs (Issues) to our open source projects:
gitlab.com/tangibleai/maitag
gitlab.com/tangibleai/nudger
gitlab.com/tangibleai/stigsite
gitlab.com/tangibleai/qary

