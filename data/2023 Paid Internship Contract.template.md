# {company_name} {cohort_name} Internship

Hi {name},

{prologue}We are pleased to offer you a paid internship at {company_name}!
Your Internship starts *{start_date}* and ends *{end_date}*.

If you would like to join us read the contract below and fill out your name and address at the end.
Check the checkboxes at the end of this agreement so that we know you understand the key points of this agreement.
To mark a checkbox, put an "X" or "x" between the square brackets ("[ ]").

To electronically sign this Internship Agreement, reply to this email.
Edit the agreement below by overwriting the three __ALLCAPS_BOLDED__ placeholders in the checklist at the bottom of the agreement with your information: __FULL_NAME__, __EMAIL_ADDRESS__, and __DATE_SIGNED__ .

-------------------------------------------------------------------------------

# {company_name} {cohort_name} Internship Agreement

This Internship Agreement is between {company_name} LLC ("{company_name}") at 3151 Via Alicante, #225, La Jolla, CA, 92037 and {full_name} ("{name}") at {email}.

The purpose of this Internship is to allow {name} to gain professional experience working on real-world projects according to the following terms and conditions:

### 1. Term:

The Internship begins *{start_date}* and concludes *{end_date}*, unless the Internship ends earlier
according to other provisions in this agreement.

### 2. Purpose:

During the Term, {company_name} will provide {name} with mentorship and learning opportunities. {company_name} will continuously adjust the activities and assignments of {name} according to mutually agreed upon goals.

### 3. Responsibilities:

During the Term, {name}'s duties and responsibilities include:

    (a) Honesty, courtesy, and kindness with everyone you interact with
    (b) Attend 1 hr of meetings per week with mentors and peers at {company_name}
    (c) Provide weekly "sprint plans" in writing (preferrably markdown checklists in a public git repository) 
    (d) Provide weekly verbal reports on your progress on your sprint plan
    (e) At the conclusion of any minimally useful software module of feature, provide a 5 minute verbal and written report (in markdown) to the {company_name} team
    (f) For _fixed price_ projects {name} will provide at least one acceptance test in Python doctest format (to ensure mutual understanding of scope)
    (g) For _hourly rate_ projects {name} will enter their billed hours within UpWork weekly and within {company_name}'s system monthly
    (h) {name} will comply with all tax and employment regulations at their location throughout the Internship
    (i) {name} will maintain a public UpWork account profile in good standing throughout the Internship

### 4. Tasks:

Throughout the Internship {name} and {company_name} will jointly establish a schedule of meetings, tasks, and mutually beneficial open source deliverables.

### 5. Intern position:

{name} agrees that they are not a permanent employee of {company_name} but are participating in a temporary Internship for education.

### 6. Acknowledgments and Representations:

There is no expectation that the Internship will result in {company_name} employing {name} permanently.
And {company_name} will expend signigicant resources educating {name}.
Those resources may exceed any benefit to {company_name}'s business activities.

### 7. Termination:

Both {company_name} and {name} retain the right to end the Internship without advance notice.

### 8. Default to Open:

{company_name} defaults to open.
Unless restricted by customer contracts or fiducial responsibility to protect user data, all {company_name} software, concepts, data and designs are licensed under a free and open source license such as [AGPL-3.0 (GNU Affero General Public License)](https://www.gnu.org/licenses/agpl-3.0.en.html) or the [Hippocratic License (MIT + First Do No Harm)](https://firstdonoharm.dev/). Interns, alumni, employees, contractors, and public contributors may freely share and reuse all open source licensed designs, algorithms, data and software packages.
Listed here are examples of free open source software packages that {company_name} supports: [gitlab.com/tangibleai/community](http://gitlab.com/tangibleai/community/) 

### 9. User Data:

In the course of performing their duties under the agreement {name} might have access to data from the users and customers interacting with {company_name}'s Products ("User Data").
All right, title, and interest in User Data will remain the property of {company_name}, unless otherwise contracted.
{name} has no intellectual property rights or other claim to User Data that is hosted, stored, or transferred to and from the Products operated or maintained on behalf of {company_name}.
{name} will cooperate with {company_name} to protect the User Data and the users' right to privacy. {name} will promptly notify {company_name} if {pronoun_is__lower} aware of any potential infringement of those rights in accordance with the provisions of this Agreement.

### 10. Standard of Care:

{name} acknowledges and agrees that, in the course of performing their duties under the agreement, {name} may receive or have access to Personal Information. {name} shall comply with the terms and conditions set forth in this Agreement in its collection, receipt, transmission, storage, disposal, use and disclosure of such Personal Information and be responsible for the unauthorized collection, receipt, transmission, access, storage, disposal, use and disclosure of Personal Information under control
In recognition of the foregoing, {name} agrees to:

    (a) keep and maintain all Personal Information in strict confidence, using such degree of care as is appropriate to avoid unauthorized access, use or disclosure;
    (b) use and disclose Personal Information solely and exclusively for the purposes for which the Personal Information, or access to it, is provided pursuant to the terms and conditions of this Agreement, and not use, sell, rent, transfer, distribute, or otherwise disclose or make available Personal Information for {name}'s own purposes without Customer's prior written consent; and
    (c) not, directly or indirectly, disclose Personal Information to any person other than {company_name}'s authorized persons, including any subcontractors, agents, outsourcers or auditors (an "Unauthorized Third Party"), without express written consent from {company_name}, unless and to the extent required by Government Authorities or as otherwise, to the extent expressly required, by applicable law.

### 11. Information Security:

{name} represents and warrants that their collection, access, use, storage, disposal and disclosure of Personal Information does and will comply with all applicable federal and state privacy and data protection laws, as well as all other applicable regulations and directives.
{name} shall implement administrative, physical and technical safeguards to protect Personal Information that are no less rigorous than accepted industry practices.

At a minimum, {name}'s safeguards for the protection of Personal Information shall include:

    (a) securing personal computer limiting access of Personal Information to Authorized Persons securing information transmission, storage and disposal; and
    (b) implementing authentication and access controls within applications that are used to store and access the Personal Information

### 12. Applicable Law:

This Agreement will be governed by California law, without giving effect to conflict of laws principles.

## Electronic Signatures

----

- [x] Agreed to by {manager_name} ({manager_email}) on {manager_date_signed}

----

- [ ] Agreed to by __FULL_NAME__ (__EMAIL_ADDRESS__) on __DATE_SIGNED__
- [ ] I want to learn about the activities and technology of {company_name} and help others with what I learn.
- [ ] I have a password-protected PC I will use to store {company_name} data. Automatic screen lock is enabled.
- [ ] I will use a browser, such as Firefox, with at least **_Standard_ level security** protections enabled.
- [ ] I agree to make my best effort to attend at least one meeting with a {company_name} representative weekly.

