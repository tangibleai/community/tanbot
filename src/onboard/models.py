from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse


class Message(models.Model):
    slack_channel = models.TextField(default='Tangible AI')
    sent_from = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    sent_to = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name='sent_to')
    date_scheduled = models.DateTimeField(default=timezone.now)
    date_sent = models.DateTimeField(default=timezone.now)
    message_title = models.TextField(blank=True, null=True)
    message_text = models.TextField(default='')

    def __str__(self):
        return self.message_title or self.message_text

    def get_absolute_url(self):
        return reverse('onboard:onboard-tanbot')


class SheduledMessage(models.Model):
    scheduled_time = models.DateTimeField()
    message_title = models.TextField(default='')
    message_text = models.TextField(default='')
    sent_to = models.ForeignKey(User, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.message_title
