# Generated by Django 3.0.7 on 2021-02-01 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('onboard', '0002_auto_20201231_2340'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='date_sent',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
