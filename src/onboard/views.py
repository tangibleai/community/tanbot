import json
import markdown
import os
import pathlib
import slack
import re

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import CreateView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from qary.skills import eliza  # ,qa
from slack_sdk import WebClient

from .models import Message, SheduledMessage
# from users.models import Profile

SLACK_BOT = '<@U01JBL45U3Z>'


@login_required
def home(request):
    user = request.user
    chat_messages = Message.objects.filter(sent_from=user).order_by('-date_sent')[:3]
    template = 'onboard/home.html'
    return render(request, template, {'chat_messages': chat_messages, 'user': user})


class MessageCreateView(LoginRequiredMixin, CreateView):
    model = Message
    fields = ['message_text']
    context_object_name = 'chat_messages'
    ordering = ["-date_sent"]

    def form_valid(self, form):
        form.instance.sent_from = self.request.user
        return super().form_valid(form)

    def render_to_response(self, context, *args, **kwargs):
        user = self.request.user
        context['user'] = user
        context['sent_from'] = Message.objects.filter(sent_to=user).all()
        context['sent_to'] = Message.objects.filter(sent_from=user).all()

        if context['sent_from'] | context['sent_to']:
            context['all_messages_of_user'] = (context['sent_from'] | context['sent_to']).order_by('-date_sent')
            context['last_messages_of_user'] = (context['sent_from'] | context['sent_to']).last().message_text
        else:
            context['all_messages_of_user'] = 'No messages to display'
            context['last_messages_of_user'] = 'No messages to display'

        context['current_tanbot_message_id'] = User.objects.get(username=user).profile.latest_tanbot_message_id
        current_tanbot_message_id = context['current_tanbot_message_id']

        try:
            context['last_sheduled_message'] = SheduledMessage.objects.get(id=current_tanbot_message_id).message_text
        except SheduledMessage.DoesNotExist:
            context['last_sheduled_message'] = 'There are currently no more onboard messages to display'

        md = markdown.Markdown()
        md_to_html_last_sheduled_message = md.convert(context['last_sheduled_message'])

        tanbot_message = Message(sent_to=User.objects.get(username=user),
                                 sent_from=User.objects.get(username='Tanbot'),
                                 message_text=md_to_html_last_sheduled_message)

        if context['last_messages_of_user'] == 'onboard':
            context['test'] = 'This will be a sheduled_message'
            tanbot_message.save()
            profile = user.profile
            profile.latest_tanbot_message_id += 1
            profile.save()

        bot_name = 'eliza'
        bot_name_regex = f'^{bot_name}\\b'
        match = re.match(bot_name_regex, context['^last_messages_of_user'].strip().lower())
        if match:
            text = context['last_messages_of_user'][match.span(0)[1] + 1:].strip()
            bot_text = eliza.Skill().reply(text)[0][1]

            tanbot_message = Message(sent_to=User.objects.get(username=user),
                                     sent_from=User.objects.get(username='Tanbot'),
                                     message_text=bot_text)

            tanbot_message.save()

        # if context['last_messages_of_user'].startswith('qary '):
        #     text = context['last_messages_of_user'].split("qary ")[1]
        #     bot_text = qa.Skill().reply(text)[0][1]

        #     tanbot_message = Message(sent_to=User.objects.get(username=user),
        #                              sent_from=User.objects.get(username='Tanbot'),
        #                              message_text=bot_text)

        #     tanbot_message.save()

        return super().render_to_response(context, *args, **kwargs)


def tanbot_api(request, username):
    data = {}
    user = User.objects.get(username=username)
    # data['current_user'] = user.username
    if Message.objects.filter(sent_to=user):
        data['sent_from'] = str(Message.objects.filter(sent_to=user).last().sent_from)
        data['sent_to'] = str(Message.objects.filter(sent_to=user).last().sent_to)
        # data['date_sent'] = Message.objects.filter(sent_to=user).last().date_sent
        current_tanbot_message_id = User.objects.get(username=user).profile.latest_tanbot_message_id - 1
        try:
            data['current_sheduled_message_md'] = SheduledMessage.objects.get(id=current_tanbot_message_id).message_text
            data['current_sheduled_message_html'] = Message.objects.filter(sent_to=user).last().message_text
        except SheduledMessage.DoesNotExist:
            data['there_are_currently_no_more_onboard_messages_to_display'] = (
                "There are currently no more onboard messages to display")
    else:
        data['user_has_not_started_the_onboard_process'] = "user has not started the onboard process"
    return JsonResponse(data)


# SLACK API Configurations
if os.environ.get('VERIFICATION_TOKEN'):
    VERIFICATION_TOKEN = os.environ.get('VERIFICATION_TOKEN')
    OAUTH_ACCESS_TOKEN = os.environ.get('OAUTH_ACCESS_TOKEN')
    CLIENT_ID = os.environ.get('CLIENT_ID')
    CLIENT_SECRET = os.environ.get('CLIENT_SECRET')

if pathlib.Path('/etc/config.json').is_file():
    with open('/etc/config.json') as config_file:
        config = json.load(config_file)
    VERIFICATION_TOKEN = config['VERIFICATION_TOKEN']
    OAUTH_ACCESS_TOKEN = config['OAUTH_ACCESS_TOKEN']
    CLIENT_ID = config['CLIENT_ID']
    CLIENT_SECRET = config['CLIENT_SECRET']


@api_view(['GET', 'POST'])
def slack_verification_challenge(request):
    with open('/tmp/django_log.txt', 'a') as f_out:
        f_out.write(f'this is the request {request}')

    client = WebClient(token=OAUTH_ACCESS_TOKEN)
    slack_message = request.data

    if slack_message.get('token') != VERIFICATION_TOKEN:
        return Response(status=status.HTTP_403_FORBIDDEN)

    # verification challenge
    if slack_message.get('type') == 'url_verification':
        return Response(data=slack_message,
                        status=status.HTTP_200_OK)

    if 'event' in slack_message:
        event_message = slack_message.get('event')

        # process user's message
        user = event_message.get('user')
        text = event_message.get('text')
        channel = event_message.get('channel')
        username = f'<@{user}>'

        if not User.objects.filter(username=username).exists():
            bot_text = f'Hello <@{user}> :wave:'
            client.chat_postMessage(channel=channel, text=bot_text)

            User.objects.create_user(username=f'<@{user}>', password='1pass2word')
            # bot_text = f' I made account for you at (staging.qary.ia)'
            # client.chat_postMessage(channel=channel, text=bot_text)

            bot_text = "type 'onboard' to begin your onboarding process"
            client.chat_postMessage(channel=channel, text=bot_text)
            return Response(status=status.HTTP_200_OK)

        app_user = User.objects.get(username=username)

        if username != SLACK_BOT:
            tanbot_message = Message(sent_to=User.objects.get(username=SLACK_BOT),
                                     sent_from=app_user, message_text=text)

            tanbot_message.save()

        else:
            tanbot_message = Message(sent_to=app_user,
                                     sent_from=User.objects.get(username=SLACK_BOT),
                                     message_text=text)

            tanbot_message.save()

        if text.lower() == 'hi':
            bot_text = f'Hello <@{user}> :wave:'
            client.chat_postMessage(channel=channel, text=bot_text)

            tanbot_message = Message(sent_to=app_user,
                                     sent_from=User.objects.get(username=SLACK_BOT),
                                     message_text=bot_text)

            tanbot_message.save()
            return Response(status=status.HTTP_200_OK)

        if text.lower() == 'onboard':

            current_tanbot_message_id = User.objects.get(username=username).profile.latest_tanbot_message_id

            try:
                last_sheduled_message = SheduledMessage.objects.get(id=current_tanbot_message_id).message_text
            except SheduledMessage.DoesNotExist:
                last_sheduled_message = 'There are currently no more onboard messages to display'

            bot_text = last_sheduled_message
            client.chat_postMessage(channel=channel, text=bot_text)

            tanbot_message = Message(sent_to=app_user,
                                     sent_from=User.objects.get(username=SLACK_BOT),
                                     message_text=bot_text)

            tanbot_message.save()

            profile = app_user.profile
            profile.latest_tanbot_message_id += 1
            profile.save()

            return Response(status=status.HTTP_200_OK)

        if text.startswith('broadcast '):
            text = text.split("broadcast ")[1]
            client.chat_postMessage(channel='#interns', text=text)

            tanbot_message = Message(sent_to=User.objects.get(username=SLACK_BOT),
                                     sent_from=User.objects.get(username=SLACK_BOT),
                                     message_title='broadcast', message_text=bot_text)

            tanbot_message.save()

            return Response(status=status.HTTP_200_OK)

        skill = app_user.profile.qary_skills

        if text.lower() == 'stop':
            app_user.profile.qary_skills = ''
            app_user.save()
            bot_text = f'You are no longer talking with {skill.title()}.'
            client.chat_postMessage(channel=channel, text=bot_text)
            return Response(status=status.HTTP_200_OK)

        if skill == 'eliza':
            bot_text = eliza.Skill().reply(text)[0][1]
            client.chat_postMessage(channel=channel, text=bot_text)
            return Response(status=status.HTTP_200_OK)

        if text.lower() == 'eliza':
            app_user.profile.qary_skills = 'eliza'
            app_user.save()
            bot_text = f'You are now talking with Eliza. If you wish to stop, type "stop".'
            client.chat_postMessage(channel=channel, text=bot_text)
            return Response(status=status.HTTP_200_OK)

        # if text.startswith('eliza '):
        #    text = text.split("eliza ")[1]
        #    bot_text = eliza.Skill().reply(text)[0][1]
        #    client.chat_postMessage(channel=channel, text=bot_text)

        #    tanbot_message = Message(sent_to=User.objects.get(username=SLACK_BOT),
        #                 sent_from=app_user,
        #                 message_text=text)

        #    tanbot_message.save()

        #    tanbot_message = Message(sent_to=app_user,
        #                     sent_from=User.objects.get(username=SLACK_BOT),
        #                     message_text=bot_text)

        #    tanbot_message.save()

        #    return Response(status=status.HTTP_200_O

    return Response(status=status.HTTP_200_OK)


class BroadcastMessageCreateView(LoginRequiredMixin, CreateView):
    model = Message
    template_name = 'onboard/broadcast_message_form.html'
    fields = ['message_text']
    context_object_name = 'chat_messages'
    ordering = ["-date_sent"]

    def form_valid(self, form):
        form.instance.sent_from = User.objects.get(username=SLACK_BOT)
        form.instance.message_title = 'broadcast'
        client = slack.WebClient(token=OAUTH_ACCESS_TOKEN)
        client.chat_postMessage(channel='#interns', text=form.instance.message_text)
        return super().form_valid(form)

    def render_to_response(self, context, *args, **kwargs):
        if Message.objects.filter(sent_from=User.objects.get(username=SLACK_BOT),
                                  message_title='broadcast'):
            context['latest_internbot_message'] = Message.objects.filter(sent_from=User.objects.get(username=SLACK_BOT),
                                                                         message_title='broadcast').last()
        else:
            context['latest_internbot_message'] = 'No Broadcasts HaveBeen Made Yet.'

        # client = WebClient(token=OAUTH_ACCESS_TOKEN)
        # client.chat_postMessage(channel='#interns', text=context['latest_internbot_message'])

        return super().render_to_response(context, *args, **kwargs)
