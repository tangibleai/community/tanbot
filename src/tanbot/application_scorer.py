""" Functions to process exported/downloaded AirTable summer internship applications 2021

1) visit airtable for intern applications: https://airtable.com/tblzVqdlC6VuhCddp/viwXsr0NLZAvSsBqz?blocks=hide
2) click tripple dot to the left on airtable to right of "Grid view" at top of "Application Form Submissions" table

>>> df_applications = pd.read_csv(Path(
...     '~/Downloads/Application Form Submissions-Grid view.csv'
...     ).expanduser().resolve())
>>> lod = list(df_applications.T.to_dict().values())
>>> yaml.dump(lod, open('applications.yaml', 'w'))

"""
import copy
import datetime
import yaml
import logging
from pathlib import Path
import pandas as pd

from tanbot.constants import DATA_DIR, YEAR, QUARTER, SEASONS
from tanbot.constants import ALL_INTERNSHIPS_FILEPATH

# '/home/hobs/code/tangibleai/internal/team/applications/Applicants-All Fields.csv'
from tanbot.constants import APPLICATIONS_AIRTABLE_FILEPATH
from tanbot.constants import APPLICATIONS_PROCESSED_FILENAME  # 'Applicants-processed.csv'
from tanbot.constants import APPLICATIONS_PROCESSED_FILEPATH  # 'Applicants-processed.csv'

from tanbot.constants import APPLICATIONS_ANNONYMIZED_FILEPATH


log = logging.getLogger(__name__)


def sum_scores(apps=APPLICATIONS_ANNONYMIZED_FILEPATH):
    """ Sum up the hand-scored applications

    Input:
      apps (list of dicts)
    Output:
      scored_apps (list of dicts)

    """
    apps = get_mutable_apps(apps)
    for i, a in enumerate(apps):
        a['scores'] = a.get('scores', {})
        a['scores']['total'] = sum(v for (k, v) in a['scores'].items() if 'tota' not in k)
        tot = a['scores']['total']
        if tot > 19:
            print(i, tot)
            a['admit'] = True
        else:
            a['admit'] = False
    return apps


def rank_and_threshold(filepath=APPLICATIONS_AIRTABLE_FILEPATH, dest_filepath=None):
    """ 2022 Summer interns rank and cumsum on effort to admit only 8-10 hr of interns """
    log.error('UNTESTED!!!!')
    filepath = Path(filepath)
    dest_filepath = dest_filepath or filepath.parent / APPLICATIONS_PROCESSED_FILENAME
    df = pd.read_csv(str(filepath))
    df1 = df[df['Status'] == 'Application Reviewed'].sort_values('evaluation_total', ascending=False)
    df1['effort_hr_per_wk'] = df1['Applicant Notes'].str.split().str[0].astype(float)
    df1['cumulative_effort'] = df1['effort_hr_per_wk'].cumsum()
    df1['admit'] = df1['cumulative_effort'] < 10
    df1.to_csv(str(dest_filepath))


def update_all_internships():
    """ run this within 1 month of the start of the next internship cohort """
    df = pd.read_csv(APPLICATIONS_PROCESSED_FILEPATH)
    allint = yaml.full_load(ALL_INTERNSHIPS_FILEPATH.open())
    allint
    allint[0]
    q = copy.deepcopy(dict(allint[0]))
    q['interns'] = {}
    dt = datetime.datetime.now()
    q['year'] = int(dt.year + (dt.month + 1) / 12)
    qnum = int((dt.month + 1) / 3) + 1
    q['quarter'] = f'Q{qnum}'
    q['season'] = SEASONS[qnum - 1]
    print(q['season'])
    q['manager_date_signed'] = f'{dt.year}/{dt.month}/{dt.day}'
    print(q['manager_date_signed'])
    q['cohort_name'] = q['season'].title() + ' ' + str(q['year'])
    print(q['cohort_name'])
    finish = dt + datetime.timedelta(7 * 13)
    q['start_date'] = f'{dt.year}/{dt.month}/{dt.day}'
    q['end_date'] = f'{finish.year}/{finish.month}/{finish.day}'
    q['num_interns'] = int(df['ai_recommended_acceptance'].sum())
    for i, row in df[df['ai_recommended_acceptance']].iterrows():
        fullname = ' '.join([row['First Name'], row['Last Name']])
        q['interns'][fullname] = {}
        q['interns'][fullname]['name'] = fullname.split(' ')[0]
        q['interns'][fullname]['email'] = row['Email']
    q['contract_template'] = 'data/Internship Contract.template.md'
    allint = [q] + allint
    yaml.dump(allint, ALL_INTERNSHIPS_FILEPATH.open('w'))
    allint = yaml.full_load(ALL_INTERNSHIPS_FILEPATH.open())
    return allint


def quarter2season(quarter):
    try:
        quarter = int(float(quarter))
    except (ValueError, TypeError):
        return str(quarter).strip().lower()
    if isinstance(quarter, (int, float)):
        return SEASONS[quarter - 1]


def get_yamlpath(year=YEAR, quarter=QUARTER, annonymize=False):
    annonymized = '.annonymized' * annonymize
    season = quarter2season(quarter)
    yamlpath = Path(DATA_DIR) / f'{year}-{season}-applications{annonymized}.yml'
    if not yamlpath.is_file():
        return Path(DATA_DIR) / f'{year}-{season}-applications{annonymized}.yaml'


def apps_csv2yaml(
        filepath='~/Downloads/Application Form Submissions-Grid view.csv',
        year=YEAR, quarter=QUARTER, annonymize=False):
    """ Quarter is 1-offset """

    filepath = Path(filepath).expanduser().resolve()
    df = pd.read_csv(filepath)
    lod = list(df.T.to_dict().values())

    if annonymize:
        lod = annonymize_apps(lod)
    yamlpath = get_yamlpath(year=year, quarter=quarter, annonymize=annonymize)
    yaml.dump(lod, open(yamlpath, 'w'))
    return yamlpath


def annonymize_apps(apps, columns=('Email', 'First Name', 'Last Name')):
    for i, a in enumerate(apps):
        for c in columns:
            a[c] = None
    return apps


def get_mutable_apps(apps):
    if isinstance(apps, (Path, str)):
        with Path(apps).open() as instream:
            apps = yaml.full_load(instream)
    apps = [dict(a) for a in apps]  # ensure in-place mutability
    return apps


def app_scores_df(apps):
    apps = sum_scores(apps)
    return pd.DataFrame([(i, a['scores']['total'], a['admit']) for i, a in enumerate(apps)])


def process_airmail_downloads(year=YEAR, quarter=QUARTER):
    """ Update applications.yaml based on csv from Airtable and scores from applications.annonymized.yaml"""
    df_applications = pd.read_csv(APPLICATIONS_AIRTABLE_FILEPATH)
    lod = list(df_applications.T.to_dict().values())
    # TODO: Ask user for year and quarter (defaulting to inference from now())
    yamlpath = get_yamlpath(year=year, quarter=quarter, annonymize=False)
    annonyamlpath = get_yamlpath(year=year, quarter=quarter, annonymize=True)
    lodannon = yaml.safe_load(open(annonyamlpath))
    scoredlen = len(lodannon)
    assert(all(('scores' in a and 'prosocial' in a['scores']) for a in lodannon))
    lodannon.extend([dict() for i in range(len(lod) - scoredlen)])
    assert(len(lodannon) == len(lod))
    assert(len(lodannon) > 1)
    for i, (app, scored) in enumerate(zip(lod, lodannon)):
        app.update({k: v for (k, v) in scored.items() if v is not None})
        lod[i] = app
        # scored.update(app)
        # lodannon[i] = scored
    assert(all(lodannon))
    assert(all(('scores' in a and 'prosocial' in a['scores']) for a in lod[:scoredlen]))
    yaml.dump(lod, open(yamlpath, 'w'))
    lodannon = annonymize_apps(lod)
    yaml.dump(lodannon, open(annonyamlpath, 'w'))
    return lod


def update_all_internships_file(year=YEAR, quarter=QUARTER, all_internships_filepath=ALL_INTERNSHIPS_FILEPATH):
    with open(open(all_internships_filepath), 'r') as streamin:
        lod = yaml.safe_load(streamin)
    cohort = {}
    for i in lod:
        if (int(i.get('year', 0)), str(i.get('season', '')).lower()) == (
                int(year), SEASONS[quarter].lower()):
            cohort = i
            break
    if not cohort:
        lod = [{}] + lod
        cohort = lod[0]
    return cohort


if __name__ == '__main__':
    lod = process_airmail_downloads()
