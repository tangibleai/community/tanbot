import copy
import logging
from pathlib import Path
from jinja2 import Template


from markdown import Markdown
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
import yaml

from tanbot import constants

log = logging.getLogger(__name__)
md = Markdown(extensions=['extra', 'smarty', 'meta'], output_format='html5')


def send_emails_gmail(cohort_data=constants.COHORT_DATA, template=None):
    # add_method_runner(cohort_data, 'lower')
    pass


def extract_internship_metadata(cohort_data=constants.COHORT_DATA):
    # add_method_runner(cohort_data, 'lower')
    cohort_info = dict(((k, v) for (k, v) in cohort_data.items() if k != 'interns'))
    return cohort_info


def load_yaml(filename):
    """ Load a list or dict from a yaml file

    >>> load_yaml([])
    []
    >>> load_yaml(constants.DEFAULT_COHORT_DATA_PATH)[0]['year'] >= 2022
    True
    """
    if isinstance(filename, (list, dict)):
        return filename
    filename = Path(filename)
    if isinstance(filename, (str, Path)):
        if not Path(filename).expanduser().resolve().absolute().is_file():
            filename = constants.DATA_DIR / filename
        if not Path(filename).expanduser().resolve().absolute().is_file():
            filename = constants.PRIVATE_DATA_DIR / filename
    with filename.open() as fin:
        return yaml.safe_load(fin)


def add_method_runner(obj, method_name='lower', sep='__'):
    r""" For .format() templates that need to run .lower(), .upper() etc on dict values

    >>> add_method_runner({'x': 'Hi'}, method_name='lower')
    {'x': 'Hi', 'x__lower': 'hi'}
    >>> add_method_runner({'x': 'Hi'}, method_name='upper', sep='2')
    {'x': 'Hi', 'x2upper': 'HI'}
    """
    # this needs to be done only inside the dict() and only on the values with str keys
    orig_obj = copy.deepcopy(obj)
    if isinstance(orig_obj, dict):
        new_obj = {str(k) + sep + method_name: add_method_runner(obj, method_name=method_name) for (k, obj) in orig_obj.items()}
        new_obj = copy.deepcopy(new_obj)
        new_obj.update(orig_obj)
        return new_obj
    if hasattr(orig_obj, method_name):
        return getattr(orig_obj, method_name)()
    if isinstance(orig_obj, list):
        return [add_method_runner(obj, method_name=method_name) for obj in orig_obj]
    return orig_obj


def add_method_runners(
        obj,
        method_names=('lower', 'title', 'upper', 'strip'),
        sep='__'):
    r""" For .format() templates that need to run .lower(), .upper() etc on dict values

    >>> add_method_runners({'x': 'Hi'}, method_names='lower upper'.split())
    {'x': 'Hi', 'x__lower': 'hi', 'x__upper': 'HI'}
    >>> add_method_runner({'x': 'Hi'}, method_names=['upper'], sep='2')
    {'x': 'Hi', 'x2upper': 'HI'}
    """
    # this needs to be done only inside the dict() and only on the values with str keys
    for name in method_names:
        obj = add_method_runner(obj, method_name=name, sep=sep)
    return obj


def load_template(filename=None):
    """ open and read the f-string in the `filename` file path """
    filename = Path(filename or constants.DEFAULT_TEMPLATE_FILENAME)
    if str(filename) == filename.name and not filename.is_file():
        filename = constants.DATA_DIR / filename
    return filename.open().read()


def ensure_template_str(template):
    if isinstance(template, str) and (len(template) > 128 or '{' in template):
        return(str(template))
    return load_template(template)


def pronoun_is(pronoun):
    """ Conjugate the verb is following a pronoun

    >>> pronoun_is('She')
    'She is'
    >>> pronoun_is('they')
    'they are'
    """
    p = pronoun.lower().strip()
    if p in ('they', 'them'):
        return pronoun + ' are'
    if p in ('he', 'she', 'him', 'her', 'it'):
        return pronoun + ' is'
    return pronoun + ' is'


def pronoun_object(pronoun):
    """ Conjugate a pronoun subject to be used as the object in a sentence

    >>> 'Give it to ' + pronoun_object('She') + '.'
    'Give it to her.'
    """
    p = pronoun.lower().strip()
    if p in ('they', 'them'):
        return 'them'
    if p in ('he', 'him'):
        return 'him'
    if p in ('it', "it's"):
        return 'it'
    if p in ('she', 'her'):
        return 'her'
    return 'her'


def send_emails_sendgrid(
        cohort_data=constants.COHORT_DATA,
        sendgrid_api_key=constants.SENDGRID_API_KEY,
        template=None, debug=constants.DEBUG):
    # cohort_data = add_method_runner(cohort_data)
    # cohort_year = cohort_data.get('year') or ''
    # cohort_season = cohort_data.get('season') or ''
    # TODO: default_template_filename=Path(
    #    f'{cohort_year} {cohort_season.title()}'.strip() + str(template)

    interns = copy.deepcopy(cohort_data['interns'])
    internship_info = extract_internship_metadata(cohort_data)
    log.warning('-' * 10)
    log.warning(f'Processing email data for {len(interns)} interns...')
    log.warning('-' * 10)
    for full_name, intern_data_raw in interns.items():
        intern_data = copy.deepcopy(internship_info)
        intern_data.update(copy.deepcopy(intern_data_raw))
        intern_data['full_name'] = full_name
        intern_data['name'] = intern_data_raw.get('name', intern_data['full_name'].split()[0])
        intern_data['pronoun'] = intern_data.get('pronoun', 'they')
        intern_data['pronoun_is'] = intern_data.get('pronoun_is', pronoun_is(intern_data['pronoun']))
        intern_data['pronoun_object'] = intern_data.get('pronoun_object', pronoun_object(intern_data['pronoun']))
        intern_data['prologue'] = Template(intern_data.get('prologue', '')).render(intern_data)
        log.warning(f'\n\nINTERN_DATA BEFORE\n{yaml.dump(intern_data)}')
        # FIXME: this is redundant with jinja2 processing of | title | upper etc
        intern_data = add_method_runners(intern_data, 'lower upper title'.split())
        # log.warning(f'\n\nINTERN_DATA AFTER_METHOD_RUNNER\n{yaml.dump(intern_data)}')
        personalized_template = intern_data.get('template', None) or template
        template = ensure_template_str(template=personalized_template)
        template_lines = template.splitlines()
        first_line = Template(template_lines[0]).render(intern_data)
        # log.warning(intern_data['name'])
        subject = intern_data.get('subject', '').format(**intern_data)
        start_linenum = 0
        # log.warning(intern_data['name'])
        if first_line.startswith('#') or (len(first_line) < 128 and len(first_line)):
            subject = subject or first_line.lstrip('#').strip()
            start_linenum = 1
        # log.warning(intern_data['name'])
        subject = subject or Template("{{name}}'s {{cohort_name}} Internship at {{company_name}}").render(intern_data)
        log.warning(f'\n\nINTERN_DATA_AFTER_UPDATE\n{yaml.dump(intern_data)}')
        body = []
        for i, line in enumerate(template_lines[start_linenum:]):
            try:
                body.append(Template(line).render(intern_data))
            except Exception:
                log.error(f'template[{i}]: {line}')
                raise
        body = md.convert('\n'.join(body))
        to_emails = [intern_data['email']] + list(intern_data['mentor_emails'])
        message = Mail(
            from_email=(intern_data.get('from_email') or constants.DEFAULT_FROM_EMAIL),
            to_emails=to_emails,
            subject=subject,
            # plain_text_content=body
            html_content=body)
        # import ipdb
        # ipdb.set_trace()
        test_email = intern_data.get('test_email', 'engineering+test@tangibleai.com')
        log.warning('=' * 0)
        log.warning(f'FROM: {message.from_email.email}')
        log.warning(f'SUBJECT: {message.subject}')
        log.warning(f'TO: {to_emails}')
        log.warning('BODY:')
        log.warning('\n'.join((message._contents[0].content or '').splitlines()))
        log.warning(f'TO: {to_emails}')
        log.warning('=' * 80 + '\n')
        log.warning(f"sendgrid_api_key={sendgrid_api_key[:5]}")
        sg = SendGridAPIClient(sendgrid_api_key)
        log.warning(f"sendgrid_client={sg}")
        try:
            yesno = input("Send this e-mail (or test-email)? y/t/[n]/q ").strip()
            yesno = yesno.strip().lower()[:1]
            if yesno == 'y':
                if not debug:
                    response = sg.send(message)
                    log.warning(response.status_code)
                    log.warning(response.body)
                    log.warning(response.headers)
                else:
                    log.warning(f'constants.DEBUG={debug} so not sending this message:\n {message}')
            elif yesno in 'qex':
                log.warning('Exiting')
                break
            elif yesno == 't':
                log.warning(f'Sending test e-mail to {test_email}')
                # FIXME: use message._set_emails() instead of recreating Mail() obj
                #         log.warning(dir(message))
                message = Mail(
                    from_email=(intern_data.get('from_email') or constants.DEFAULT_FROM_EMAIL),
                    to_emails=[test_email],
                    subject=subject,
                    # plain_text_content=body
                    html_content=body)
                response = sg.send(message)
                log.warning(response.status_code)
                log.warning(response.body)
                log.warning(response.headers)
            else:
                log.warning('!' * 120)
                log.warning('!!!!!!!!!!!!!!!!!!!SKIPPED SENDING EMAIL!!!!!!!!!!!!!!!!!!!!!!!')
                log.warning(f'message:\n {message}')
                log.warning('!' * 120)
        except Exception as e:
            raise e
