import os
from pathlib import Path
from sendgrid import SendGridAPIClient
# from sendgrid.helpers.mail import Mail
import yaml
from markdown import Markdown
from .gmailer import GmailAccount, build_message

md = Markdown(extensions=['extra', 'smarty', 'meta'], output_format='html5')

SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY')
REPO_DIR = Path(__file__).parent.parent.parent
DATA_DIR = Path(REPO_DIR, 'data')

DEFAULT_INTERNSHIP_FILEPATH = DATA_DIR / 'all-internships.yml'
INTERNSHIP_DATA = yaml.load(DEFAULT_INTERNSHIP_FILEPATH.open(), Loader=yaml.FullLoader)
EMAIL_BODY_FSTRING = (DATA_DIR / '2021 Spring Internship Contract.template.md').open().read()


def compose_email(intern_data=None, template=EMAIL_BODY_FSTRING):
    if not intern_data:
        intern_data = {k: v for (k, v) in INTERNSHIP_DATA[0].items() if k != "interns"}
        intern_data.update(list(INTERNSHIP_DATA[0]['interns'].items())[0][1])
        intern_data['full_name'] = list(INTERNSHIP_DATA[0]['interns'].items())[0][0]

    intern_data['prologue'] = intern_data.get('prologue', '')
    body = '\n'.join(template.format(**intern_data).splitlines()[1:])
    body = md.convert(body)
    subject = template.splitlines()[0].lstrip('#').strip().format(cohort_name=intern_data['cohort_name'])
    to_emails = [intern_data['email']] + list(intern_data['mentor_emails'])
    print('=' * 0)
    print(f'SUBJECT: {subject}')
    print(f'TO: {to_emails}')
    print('BODY:')
    print('\n'.join(body.splitlines()))
    print('=' * 80)
    print()
    print()
    # gmail = GmailAccount()
    # build_message()
    message = build_message(
        destination=intern_data['email'],
        body=body,
        subject=subject,
        attachments=[],
        our_email='engineering+tanbot@tangibleai.com')
    return message


def cli_send_emails(semester_data, template=EMAIL_BODY_FSTRING):
    # the first dictionary in the list of internships_semesters is the most recent, active semester
    this_semester = semester_data
    this_semester_info = {k: v for k, v in this_semester.items() if k != 'interns'}
    interns = this_semester['interns']

    print(f'Processing email merge data for {len(interns)} interns...')
    print()

    for full_name, intern_data in interns.items():
        default_intern_data = {}
        default_intern_data.update(this_semester_info)
        default_intern_data.update(intern_data)
        message = compose_email(intern_data=default_intern_data, template=template)
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        try:
            yesno = input("Send this e-mail? y/[N] ").strip()
            if yesno in 'yY':
                response = sg.send(message)
                print(response.status_code)
                print(response.body)
                print(response.headers)
            elif yesno in 'qQxX':
                print('Exiting')
                break
            else:
                print('!' * 120)
                print('!!!!!!!!!!!!!!!!!!!SKIPPED SENDING EMAIL!!!!!!!!!!!!!!!!!!!!!!!')
                print('!' * 120)
        except Exception as e:
            print(e.message)


if __name__ == '__main__':

    # INTERNSHIP_DATA_PATH = /home/hobs/code/tangibleai/tanbot/data/2021-01-winter-internship.yml
    # cohort_name, name, full_name, email, start_date, end_date
    cli_send_emails(semester_data=INTERNSHIP_DATA[0])
