# constants.py
import logging
from pathlib import Path
import re
import yaml
import os
import dotenv

log = logging.getLogger(__name__)
dotenv.load_dotenv()
globals().update(dict(os.environ))


def env_to_yaml(env_text):
    env_yaml = env_text
    env_yaml = re.sub(r'\bexport\b', '', env_yaml)
    env_yaml = re.sub(r'\s*=\s*', ': ', env_yaml)
    return env_yaml


def clean_dict_keys(d, lowerer=str, upperer=str.upper, replace=[' ', '_']):
    d = dict({upperer(lowerer(k.strip())).replace(*replace): v for (k, v) in d.items()})
    return d


# tanbot/src/
SRC_DIR = Path(__file__).expanduser().resolve().absolute().parent.parent
# __file__ = /home/hobs/code/tangibleai/tanbot/src/tanbot/emailer.py
# tanbot/ (containing .git/)
# sys.path.append(SRC_DIR)
REPO_DIR = SRC_DIR.parent
INTERNAL_DIR = REPO_DIR.parent.parent / 'internal'

DATA_DIR = Path(REPO_DIR, 'data').resolve().absolute()
# /home/hobs/code/tangibleai/internal/team/interns/data/all-internships.yml
PRIVATE_DATA_DIR = Path(INTERNAL_DIR, 'team', 'interns', 'data').resolve().absolute()

# '/home/hobs/code/tangibleai/internal/team/applications/Applicants-All Fields.csv' or 'Application Form Submissions-Grid view.csv'
APPLICATIONS_FILENAME = 'Applicants-All Fields.csv'
APPLICATIONS_PROCESSED_FILENAME = 'Applicants-processed.csv'
APPLICATIONS_AIRTABLE_FILEPATH = (
    PRIVATE_DATA_DIR / APPLICATIONS_FILENAME).expanduser().resolve()
APPLICATIONS_PROCESSED_FILEPATH = (
    PRIVATE_DATA_DIR / APPLICATIONS_PROCESSED_FILENAME).expanduser().resolve()
APPLICATIONS_ANONYMIZED_FILEPATH = Path(DATA_DIR) / 'applications.annonymized.yaml'
ALL_INTERNSHIPS_FILEPATH = PRIVATE_DATA_DIR / 'all-internships.yml'
if not ALL_INTERNSHIPS_FILEPATH.is_file():
    ALL_INTERNSHIPS_FILEPATH = Path(DATA_DIR) / 'all-internships.yaml'
SEASONS = 'winter spring summer fall'.split()
# QUARTER = 3  # 1 = winter, 4 = fall
# YEAR = 2021

DEBUG = int(os.environ.get('DEBUG') or 0)
# INTERNSHIP_DATA_PATH = /home/hobs/code/tangibleai/tanbot/data/2021-01-winter-internship.yml
# cohort_name, name, full_name, email, start_date, end_date
DEFAULT_COHORT_DATA_PATH = PRIVATE_DATA_DIR / 'all-internships.yml'
INTERNSHIP_DATA = yaml.full_load(DEFAULT_COHORT_DATA_PATH.open())
COHORT_DATA = INTERNSHIP_DATA[0]
# SENDGRID_API_KEY = os.environ.get('SENDGRID_API_KEY')
# TEMPLATE_PATH = /home/hobs/code/tangibleai/tanbot/data/2021-01-15 Winter 2021 Internship Contract.template.md
# /home/hobs/code/tangibleai/tanbot/data/2021 Spring Internship Contract.template.md
DEFAULT_TEMPLATE_FILENAME_PREFIX = ''
DEFAULT_FROM_EMAIL = 'engineering+tanbot@tangibleai.com'
DEFAULT_TEMPLATE_FILENAME_SUFFIX = 'Internship Contract.template.md'
DEFAULT_TEMPLATE_FILENAME = Path(DEFAULT_TEMPLATE_FILENAME_SUFFIX)
DATED_TEMPLATE_FILENAME = Path(' '.join(
    (DEFAULT_TEMPLATE_FILENAME_PREFIX.strip(),
        DEFAULT_TEMPLATE_FILENAME_SUFFIX.strip())))
DEFAULT_TEMPLATE_FILEPATH = DATA_DIR / DEFAULT_TEMPLATE_FILENAME
log.warning(f'DEFAULT_TEMPLATE_FILENAME:\n  {DEFAULT_TEMPLATE_FILENAME}')

DEFAULT_WELCOME_TEMPLATE_FILENAME = 'Internship Welcome.template.md'
DEFAULT_WELCOME_TEMPLATE_FILEPATH = DATA_DIR / DEFAULT_WELCOME_TEMPLATE_FILENAME
log.warning(f'DEFAULT_WELCOME_TEMPLATE_FILENAME:\n  {DEFAULT_WELCOME_TEMPLATE_FILENAME}')

assert DATA_DIR.is_dir()
assert PRIVATE_DATA_DIR.is_dir()
assert (REPO_DIR / '.git').is_dir()
