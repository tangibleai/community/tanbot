###########
# BUILDER #
###########

# pull official base image
FROM python:3.7 as builder

# set work directory
WORKDIR /tanbot

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# lint
RUN pip install --upgrade pip
RUN pip install flake8
COPY . .
RUN flake8 --ignore=E501,F401,F541 .

# install psycopg2 dependencies
RUN apt-get update
RUN apt-get install python3-dev libpq-dev -y

# install dependencies
COPY ./requirements.txt .
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /tanbot/wheels -r requirements.txt


#########
# FINAL #
#########

# pull official base image
FROM python:3.7

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
RUN addgroup --system app && adduser --system app --ingroup app

# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/static
COPY ./src/onboard/static $APP_HOME/static
WORKDIR $APP_HOME

# install psycopg2 dependencies
RUN apt-get update
RUN apt-get install python3-dev libpq-dev -y

# install dependencies
COPY --from=builder /tanbot/wheels /wheels
COPY --from=builder /tanbot/requirements.txt .
RUN pip install --no-cache /wheels/*

# copy entrypoint-prod.sh
COPY ./scripts/entrypoint.prod.sh $APP_HOME

# copy project
COPY . $APP_HOME
#COPY --chown=app:app . $APP_HOME
RUN pip install -e .

# chown all the files to the app user
#COPY --chown=app:app . $APP_HOME
#RUN chown app:app -R $APP_HOME
#RUN chown -R app:app $APP_HOME

# change to the app user
#USER app

# run entrypoint.prod.sh
ENTRYPOINT ["/home/app/web/scripts/entrypoint.prod.sh"]
